package com.example.weibossina;




import com.sina.weibo.sdk.api.TextObject;
import com.sina.weibo.sdk.api.WeiboMultiMessage;
import com.sina.weibo.sdk.api.share.BaseResponse;
import com.sina.weibo.sdk.api.share.IWeiboHandler;
import com.sina.weibo.sdk.api.share.IWeiboShareAPI;
import com.sina.weibo.sdk.api.share.SendMultiMessageToWeiboRequest;
import com.sina.weibo.sdk.api.share.WeiboShareSDK;
import com.sina.weibo.sdk.auth.AuthInfo;
import com.sina.weibo.sdk.auth.Oauth2AccessToken;
import com.sina.weibo.sdk.auth.WeiboAuthListener;
import com.sina.weibo.sdk.constant.WBConstants;
import com.sina.weibo.sdk.exception.WeiboException;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.Toast;

public class MainActivity extends Activity implements IWeiboHandler.Response{
	

	private final String TAG = "WBShareActivity";
	private final String KEY_SHARE_TYPE = "key_share_type";
	private final int SHARE_CLIENT = 1;
	private final int SHARE_ALL_IN_ONE = 2;

	/** 微博微博分享接口实例 */
	private IWeiboShareAPI  mWeiboShareAPI = null;

	private int mShareType = SHARE_CLIENT;

	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	
    	setContentView(R.layout.activity_main);
        
    	mWeiboShareAPI = WeiboShareSDK.createWeiboAPI(this, Constants.APP_KEY);

		sina(savedInstanceState);
    	
		((ImageButton) findViewById(R.id.button1))
				.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						
						sendSingleMessage();
//						startActivity(new Intent(MainActivity.this,
//								WBShareMainActivity.class));
					}
				});
	}
	
	
	
	
	/**
	 * 初始化 UI 和微博接口实例 。
	 */
	private void sina(Bundle savedInstanceState){


		mShareType = getIntent().getIntExtra(KEY_SHARE_TYPE, SHARE_CLIENT);
		// 创建微博分享接口实例
		mWeiboShareAPI = WeiboShareSDK.createWeiboAPI(this, Constants.APP_KEY);

		// 获取微博客户端相关信息，如是否安装、支持 SDK 的版本
		Toast.makeText(this, "获取微博客户端相关信息，如是否安装、支持 SDK 的版本\n"
				+ mWeiboShareAPI.isWeiboAppInstalled() +"\n"
				+ mWeiboShareAPI.getWeiboAppSupportAPI()+"  "+mShareType
				, Toast.LENGTH_LONG).show();

		// 注册第三方应用到微博客户端中，注册成功后该应用将显示在微博的应用列表中。
		// 但该附件栏集成分享权限需要合作申请，详情请查看 Demo 提示
		// NOTE：请务必提前注册，即界面初始化的时候或是应用程序初始化时，进行注册
		mWeiboShareAPI.registerApp();

		// 当 Activity 被重新初始化时（该 Activity 处于后台时，可能会由于内存不足被杀掉了），
		// 需要调用 {@link IWeiboShareAPI#handleWeiboResponse} 来接收微博客户端返回的数据。
		// 执行成功，返回 true，并调用 {@link IWeiboHandler.Response#onResponse}；
		// 失败返回 false，不调用上述回调
		if (savedInstanceState != null) {
			mWeiboShareAPI.handleWeiboResponse(getIntent(), this);
		}
	}


	/**
	 * 第三方应用发送请求消息到微博，唤起微博分享界面。
	 * 当{@link IWeiboShareAPI#getWeiboAppSupportAPI()} < 10351 时，只支持分享单条消息，即
	 * 文本、图片、网页、音乐、视频中的一种，不支持Voice消息。
	 * 
	 * @param hasText    分享的内容是否有文本
	 * @param hasImage   分享的内容是否有图片
	 * @param hasWebpage 分享的内容是否有网页
	 * @param hasMusic   分享的内容是否有音乐
	 * @param hasVideo   分享的内容是否有视频
	 */
	private void sendSingleMessage() {
		Toast.makeText(this, "按下分享"
				, Toast.LENGTH_LONG).show();

		// 1. 初始化微博的分享消息
		WeiboMultiMessage weiboMessage = new WeiboMultiMessage();
		weiboMessage.textObject = getTextObj();

		// 2. 初始化从第三方到微博的消息请求
		SendMultiMessageToWeiboRequest request = new SendMultiMessageToWeiboRequest();
		// 用transaction唯一标识一个请求
		request.transaction = String.valueOf(System.currentTimeMillis());
		request.multiMessage = weiboMessage;

		// 3. 发送请求消息到微博，唤起微博分享界面
		AuthInfo authInfo = new AuthInfo(this, Constants.APP_KEY, Constants.REDIRECT_URL, Constants.SCOPE);
		Oauth2AccessToken accessToken = AccessTokenKeeper.readAccessToken(getApplicationContext());
		String token = "";
		if (accessToken != null) {
			token = accessToken.getToken();
		}

		Toast.makeText(getApplicationContext(), "onAuthorizeComplete token = " + token, 0).show();
		mWeiboShareAPI.sendRequest(this, request, authInfo, token, new WeiboAuthListener() {

			@Override
			public void onWeiboException( WeiboException arg0 ) {
			}

			@Override
			public void onComplete( Bundle bundle ) {
				// TODO Auto-generated method stub
				Oauth2AccessToken newToken = Oauth2AccessToken.parseAccessToken(bundle);
				AccessTokenKeeper.writeAccessToken(getApplicationContext(), newToken);
				Toast.makeText(getApplicationContext(), "onAuthorizeComplete token = " + newToken.getToken(), 0).show();
			}

			@Override
			public void onCancel() {
			}
		});

	}

	/**
	 * @return 商品說明文
	 */
	private TextObject getTextObj() {
		TextObject textObject = new TextObject();
		textObject.text = "測試分享";
		return textObject;
	}

	/**
	 * 結束之後回傳訊息
	 */
	@Override
	public void onResponse(BaseResponse baseResp) {
		if(baseResp!= null){
			switch (baseResp.errCode) {
				case WBConstants.ErrorCode.ERR_OK:
					Toast.makeText(this, "分享成功", Toast.LENGTH_LONG).show();
					break;
				case WBConstants.ErrorCode.ERR_CANCEL:
					Toast.makeText(this, "客户端分享，取消，保存至草稿。", Toast.LENGTH_LONG).show();
					break;
				case WBConstants.ErrorCode.ERR_FAIL:
					Toast.makeText(this, "新浪微博分享失败:" + baseResp.errMsg, Toast.LENGTH_LONG).show();
					break;
			}
		}
	}
	
	
	
	
	
	
	
	
	

}